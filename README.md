# OnePlus 9 Pro LineageOS 19.1 NetHunter Enabled
![Lineage Kali](https://gitlab.com/perelax/src_imgs/-/raw/main/Lineage%20Kali%20OnePlus/kali_lineage.png)

## Description
Custom ROM for OnePlus 9 Pro mobile phones.  The ROM is Lineage OS 19.1 (Android 12) and the kernel is configured to support Kali NetHunter.

For more information about lemonadep for OnePlus 9 Pro:    
[Info About Lemonadep](https://wiki.lineageos.org/devices/lemonadep/).


For more information about Kali NetHunter or to download, visit:  
[Kali NetHunter](https://www.kali.org/docs/nethunter/).

## Installation
The ROM is built from LineageOS source and is installed the same way as official downloads.

The ROM can be used as an 'update' image, or as a full install.  For convenience, an update.zip file containing the boot.img and android-info.txt are included in the repo.

If you need to unlock the bootloader or flash initial partitions, please read the 
[detailed installation instructions from LineageOS](https://wiki.lineageos.org/devices/lemonadep/install).

*Note* The original LineageOS install directions, as well as required dtbo and vendor-boot images can be found in the original-install.zip file.

If you are already running Lineage 19.1, then you can install from recovery using the steps below:

## Installing from Lineage Recovery
 - Plug the device into USB and place in PTP mode.
 - Start ADB
```
adb devices

```
 - Reboot into recovery.
```
adb reboot recovery

```
 - In recovery menu, select 'apply update' --> 'apply from adb'
 - Follow the prompt to sideload image:
```
adb sideload lineage-19.1-20231020-UNOFFICIAL-lemonadep.zip

```
 - Update will pause at 47%, wait for completion.
 - When step 2 completes, return to recovery menu and select 'reboot system now'.
 - Verify new image by opening Settings -> About Phone.  
 You should see '19-20230424-UNOFFICIAL-lemonadep' under LineageOS Version, see below.

## Using update.zip 
If you only wish to update certain images, you can use the fastboot update.zip method to do so.  For example:
```
adb reboot bootloader
fastboot devices
fastboot update update.zip
fastboot reboot

```

## NetHunter
Kernel is configured to support Kali Linux - NetHunter:

![Lineage Kali Screens](https://gitlab.com/perelax/src_imgs/-/raw/main/Lineage%20Kali%20OnePlus/nethunter_on_boot.jpg)

## If Using Magisk
Flashing the new boot.img will affect Magisk.  This is expected as Magisk patches the boot.img, which was replaced.  Simply re-install and all previous Magisk allowances are retained.  

For more information about Magisk or to download, vist:  
[TopJohnWu - Magisk](https://github.com/topjohnwu/Magisk).

Steps to reinstall Magisk:  
 - Uninstall Magisk.  
Uninstalling will not effect the stored database.  
Previous superuser rights given to apps will remain after reinstall.
 - Using the same steps from above, flash Magisk:
```
adb devices
adb reboot recovery
adb sideload Magisk-v26.1.zip

```
*Note* Signature verification will fail, this is expected, proceed with install.
 - Reboot system
 - Open Magisk and follow the prompts to apply update and change settings.  Open the superuser tab and verify previous permissions remain.
 - The entire process seen from terminal:
 
![Lineage Kali Screens](https://gitlab.com/perelax/src_imgs/-/raw/main/Lineage%20Kali%20OnePlus/screens_1.png)

## Selecting your own theme colors
Users can bypass system theming with Monet byt using 'pThemer':

![Lineage Kali Screens](https://gitlab.com/perelax/src_imgs/-/raw/main/Lineage%20Kali%20OnePlus/pThemer.jpg)

pThemer works with bundled app 'Chalice', a simple content provider facilitating communication between layers during boot.  

## Support

For issues, comments and questions, please visit the [OnePlus 9 and 9 Pro Lineage 19.1 Nethunter Enabled repository](https://gitlab.com/perelax/oneplus-9-9pro-lineage-19.1-nethunter-enabled).
  
Feel like saying thank you?  

[![Buy Me A Coffee](https://cdn.buymeacoffee.com/buttons/default-orange.png){width=120}](https://www.buymeacoffee.com/perelax)

## Authors
Perelax

## License
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

See <https://www.gnu.org/licenses/> for a copy of the GNU General Public License.
